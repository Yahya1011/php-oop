<?php
// Bentuk statment
include 'animal.php';
include 'Frog.php';
include 'Ape.php';
?>

<?php
$sheep = new Animal("shaun");
echo "Name : " . $sheep->name . "<br>";
echo "Legs : " . $sheep->legs . "<br>";
echo "Cold Blooded : " . $sheep->cold_blooded . "<br>";

$kodok = new Frog("buduk");
echo "<br> Name : " . $kodok->name . "<br>";
echo "Legs : " . $kodok->legs . "<br>";
echo "Cold Blooded : " . $kodok->cold_blooded . "<br>";
echo "Jump : ";
$kodok->Jump();

$sungokong = new Ape("kera sakti");
echo "<br> Name : " . $sungokong->name . "<br>";
echo "Legs : " . $sungokong->legs . "<br>";
echo "Cold Blooded : " . $sungokong->cold_blooded . "<br>";
echo "Yell : ";
$sungokong->yell();
?>